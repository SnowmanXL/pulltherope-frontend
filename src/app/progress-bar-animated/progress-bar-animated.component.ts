import { Component, OnInit } from '@angular/core';
import {NgbProgressbarConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-progress-bar-animated',
  templateUrl: './progress-bar-animated.component.html',
  styleUrls: ['./progress-bar-animated.component.css']
})
export class ProgressBarAnimatedComponent implements OnInit {

  constructor(config: NgbProgressbarConfig) {
    // customize default values of progress bars used by this component tree
    config.max = 1000;
    config.striped = true;
    config.animated = true;
    config.type = 'success';
    config.height = '50px';
    config.showValue = true;
  }

  ngOnInit() {
  }

}
