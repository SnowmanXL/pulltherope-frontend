import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressBarAnimatedComponent } from './progress-bar-animated.component';

describe('ProgressBarAnimatedComponent', () => {
  let component: ProgressBarAnimatedComponent;
  let fixture: ComponentFixture<ProgressBarAnimatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressBarAnimatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressBarAnimatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
