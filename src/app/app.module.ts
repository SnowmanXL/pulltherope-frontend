import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { GameClientComponent } from './game-client/game-client.component';
import { GameConfigClientComponent } from './game-config-client/game-config-client.component';
import {RouterModule, Routes} from '@angular/router';
import {StompConfig, StompService} from '@stomp/ng2-stompjs';
import SockJS from 'sockjs-client';
import {GameService} from './services/game-api.service';
import {HttpClientModule} from '@angular/common/http';
import { ProgressBarAnimatedComponent } from './progress-bar-animated/progress-bar-animated.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

const appRoutes: Routes = [
  { path: '', component: GameClientComponent },
  { path: 'console', component: GameConfigClientComponent }
];

export function socketProvider() {
  return new SockJS('/pulltherope');
}

const stompConfig: StompConfig = {
  url: socketProvider,
  headers: {
    login: '',
    passcode: ''
  },
  heartbeat_in: 0,
  heartbeat_out: 20000,
  reconnect_delay: 5000,

  // Will log diagnostics on console
  debug: false
};

@NgModule({
  declarations: [
    AppComponent,
    GameClientComponent,
    GameConfigClientComponent,
    ProgressBarAnimatedComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    NgbModule.forRoot()
  ],
  providers: [
    StompService,
    {
      provide: StompConfig,
      useValue: stompConfig
    },
    GameService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
