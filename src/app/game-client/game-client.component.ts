import {Component, OnDestroy, OnInit} from '@angular/core';
import {StompService} from '@stomp/ng2-stompjs';
import {Message} from '@stomp/stompjs';
import {Subscription} from 'rxjs/Subscription';
import {Observable} from 'rxjs/Observable';
import {GameService} from '../services/game-api.service';

@Component({
  selector: 'app-game-client',
  templateUrl: './game-client.component.html',
  styleUrls: ['./game-client.component.css']
})
export class GameClientComponent implements OnInit, OnDestroy {

  // Subscription status
  public subscribed: boolean;
  private subscription: Subscription;
  public messages: Observable<Message>;
  public teamName: string;
  public showWinImage = false;

  ngOnInit(): void {
    this.subscribed = false;
    this.getTeamName();
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }

  constructor(private _stompService: StompService, private stageServices: GameService) { }

  public subscribe() {
    if (this.subscribed) {
      return;
    }
    this.messages = this._stompService.subscribe('/topic/game', { team: this.teamName });
    this.subscription = this.messages.subscribe(this.on_next);
    this.subscribed = true;
  }

  public unsubscribe() {
    if (!this.subscribed) {
      return;
    }

    // This will internally unsubscribe from Stomp Broker
    // There are two subscriptions - one created explicitly, the other created in the template by use of 'async'
    this.subscription.unsubscribe();
    this.subscription = null;
    this.messages = null;
    this.subscribed = false;
  }

  public on_next = (message: Message) => {
    // console.log(message);

    const msg = JSON.parse(message.body);

    switch (msg.type) {
      case 'End':
        this.showWinImage = (this.teamName === msg.resp);
        break;
      case 'Game_Lifecycle':
        if (this.showWinImage === true) {
          this.showWinImage = !('reset' === msg.resp);
        }
        break;
    }


  }

  public score() {
    this._stompService.publish('/app/click', `{"team": "${this.teamName}"}`);
  }


  public getTeamName() {
    this.stageServices.getTeam().subscribe(data => {
      this.teamName = data.resp;
      this.subscribe();
    });
  }

}
