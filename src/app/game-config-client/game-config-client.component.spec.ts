import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameConfigClientComponent } from './game-config-client.component';

describe('GameConfigClientComponent', () => {
  let component: GameConfigClientComponent;
  let fixture: ComponentFixture<GameConfigClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameConfigClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameConfigClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
