import {Component, OnDestroy, OnInit} from '@angular/core';
import {StompService} from '@stomp/ng2-stompjs';
import {GameService} from '../services/game-api.service';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {Message} from '@stomp/stompjs';
import {Subject} from 'rxjs/Subject';
import {debounceTime} from 'rxjs/operator/debounceTime';
import {NgbProgressbarConfig} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-game-config-client',
  templateUrl: './game-config-client.component.html',
  styleUrls: ['./game-config-client.component.css']
})
export class GameConfigClientComponent implements OnInit, OnDestroy {

  // Subscription status
  public subscribed: boolean;
  private subscription: Subscription;
  public messages: Observable<Message>;
  public gamehasstarted: boolean;
  public showalert = false;
  private _success = new Subject<string>();
  successMessage: string;

  public teamAvalue= 0;
  public teamBvalue= 0;

  ngOnInit(): void {
    this.subscribed = false;
    this.subscribe();

    this._success.subscribe((message) => {
      this.successMessage = message;
      this.showalert = true;
    });
    debounceTime.call(this._success, 5000).subscribe(() => this.showalert = false);
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }

  constructor(private _stompService: StompService, private stageServices: GameService, config: NgbProgressbarConfig) {
    config.max = 1000;
    config.striped = true;
    config.animated = true;
    config.type = 'success';
    config.height = '150px';
    config.showValue = true;
  }

  public subscribe() {
    if (this.subscribed) {
      return;
    }
    this.messages = this._stompService.subscribe('/topic/game');
    this.subscription = this.messages.subscribe(this.on_next);
    this.subscribed = true;
  }

  public unsubscribe() {
    if (!this.subscribed) {
      return;
    }

    // This will internally unsubscribe from Stomp Broker
    // There are two subscriptions - one created explicitly, the other created in the template by use of 'async'
    this.subscription.unsubscribe();
    this.subscription = null;
    this.messages = null;
    this.subscribed = false;
  }

  public on_next = (message: Message) => {
    // console.log(message.body);
    const msg = JSON.parse(message.body);

    switch (msg.type) {
      case 'Game_Score':
        this.teamAvalue = msg.teamA;
        this.teamBvalue = msg.teamB;
        break;
    }

  }

  public showAlertGameStarted() {
    // console.log('show alert!');
    this._success.next('The game has started!');
  }

  public startgame() {
    this.stageServices.startGame().subscribe(
      response => {
        // console.log('response = ' + response);
        this.gamehasstarted = (response.resp === 'true');
        this.showAlertGameStarted();
      });
  }

  public resetgame() {
    this.stageServices.resetGame().subscribe(
      response => {
        // console.log('response = ' + response);
        this.gamehasstarted = (response.resp === 'false');
      });
  }


}
